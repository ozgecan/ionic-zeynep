angular.module('starter.services', [])
.factory('sessionservice',function($http){
	return{
		set:function(key,value){
			return localStorage.setItem(key,value);//kayıt
		},
		get:function(key){
			return localStorage.getItem(key);//okuma
		},
		destroy:function(key){
			return localStorage.removeItem(key); //silme
		}
	}	
})
.factory('MyService', function ($location,sessionservice,$rootScope) {
  return {
    degerKontrol: function (deger,no) {//hangi sayfaya yönlendireceği bilgisi için gerekli sayı değeri
	if(deger==1){
	sessionservice.set("user",no);
   }else{
		$location.path('search');
	}
  },
  islogged:function(){
	var dg=sessionservice.get("user");
	return dg;
	},
	lougout:function(){
		var d=sessionservice.destroy("user");
	       return d;
	}
  }
})

 





