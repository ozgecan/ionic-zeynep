// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

angular.module('starter', ['ionic', 'starter.controllers','starter.services','ui.bootstrap','firebase'])
.run(function($ionicPlatform,$location,$rootScope,MyService,$window,sessionservice) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
  $rootScope.$on('$locationChangeStart',function(){
  var te=MyService.islogged();//localstoragede ki deger var
	if(te){ //localstoragede bu veri zaten var
		$rootScope.sifre=te;
	}

  })
  })

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
	//ben yaptım
	 .state('browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html',
		  controller:'brws'
        }
      }
    })
	//
    .state('app.sessions', {
      url: '/sessions',
      views: {
        'menuContent': {
          templateUrl: 'templates/sessions.html',
          controller: 'AppCtrl'
        }
      }
    })
	//login.html sayfası için kontrol yağp
.state('login', {
      url: "/login",
      templateUrl: "templates/login.html",
      controller: 'AppCtrl'
    })
.state('search',{
	url:"/search",
	templateUrl:"templates/search.html",
	controller:'ctrl'
	
})	
//bu html sayfasını ben ekledim
.state('ara',{
	url:"/ara",
	templateUrl:"templates/ara.html",
	controller:'AppCtrl'	
})
//
.state('message',{
	url:"/message",
	templateUrl:"templates/message.html",
	controller:'AppCtrl'	
})
//yeni
.state('acilis',{
	url:"/acilis",
	templateUrl:"templates/acilis.html",
	controller:'AppCtrl'	
})
//
.state('gidilecekmsj',{
	url:"/gidilecekmsj",
	templateUrl:"templates/gidilecekmsj.html",
	controller:'AppCtrl'	
})

	//
  .state('app.session', {
    url: "/sessions/:sessionId",
    views: {
      'menuContent': {
        templateUrl: 'templates/session.html',
        controller: 'SessionCtrl'
		
      }
    }
  })
  // if none of the above states are matched, use this as the fallback
 // $urlRouterProvider.otherwise('/app/sessions');
    $urlRouterProvider.otherwise('search');
  //bu yukarıdaki ifadede ionic ilk açıldıgında hangi html sayfasının
  //gözükeceğini yazıyoruz
});
