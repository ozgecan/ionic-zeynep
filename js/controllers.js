angular.module('starter.controllers', ['starter.services','ui.bootstrap','firebase'])

.controller('AppCtrl', function($scope, $ionicModal, $timeout,$rootScope,MyService,$firebase,$state,$window) {
	$scope.silme=function(){
	$scope.gecici=MyService.lougout(); //localStoragedeki degeri silen fonksiyon
	$scope.sifre=$scope.gecici;	//sifre global tanımlı oldugu için ben sadece burda değişiklik yapılmasını istediğimden $scope olarak tanımladım
	$window.location.reload();//sayfanın yenilenmesi için yaptım
	

$state.go('search');//eger logout tıklandı ise direk tekrar en basa gitsin
	}
	
	//bu kısım message.html kısmı için gerekli
	$scope.girilenisim=$scope.isim1;
	$scope.oturumismi=MyService.islogged();
$scope.yeni="false";//bu değişkeni gösterme butonu için kullanıyoruz
var ref=new Firebase("https://mesajuygulama-ea72c.firebaseio.com"); //firebase bağlantısı
var sync=$firebase(ref);
$scope.chats=sync.$asArray();
$rootScope.kac=0;
$scope.sendMessage=function(){
	$rootScope.yazdir="";
	if($scope.girilenisim==$scope.oturumismi){
		var boyut=$scope.chats.length;
		var i=0;
		for(i=(boyut-1);i>=0;i--){
			if($scope.chats[i].adi!=$scope.oturumismi){
				$rootScope.yazdir=$scope.chats[i].adi;
				break;
			}
		}
	}else{
		$rootScope.yazdir=$scope.oturumismi;
	}
	$rootScope.kac+=1;
	$scope.chats.$add({
	   adi:$scope.girilenisim,
		mesg:$scope.msgctrl.msg,
		gond:$rootScope.yazdir
	});	
	$scope.msgctrl.msg="";

}
$scope.say=function(){
		$rootScope.yeni1='true';
	$scope.benzer=[];
	var t=0;
	var son=$scope.chats.length;
	if(son==0){
		$rootScope.yaz="yeni mesaj yok";
		$rootScope.yeni2="false";
	}
	$scope.sondeger=$scope.chats[son-1].adi;
	$scope.sayisonucu=0;
 if($rootScope.isim1==$scope.sondeger){
		$rootScope.yaz="yeni mesaj yok";
		$rootScope.yeni2="false";
	}else{
		var i;
		for(i=son-1;i>=0;i--){
		$scope.ilk=$scope.chats[i].adi;
		if($scope.ilk!=$rootScope.isim1){
			t++;
			$scope.sayisonucu+=1;
		}else{
			break;
		}
		}
		$rootScope.yaz=$scope.sayisonucu+"  yeni mesaj";
	$rootScope.yeni2="true";
	}
	
}
$scope.gor=function(){
$rootScope.isim1=MyService.islogged();//oturum hangi adla açıldı ise onu yaz
var boyut=$scope.chats.length;
for(i=(boyut-1);i>=0;i--){
			if($scope.chats[i].adi!=$scope.oturumismi){
				$rootScope.ilkelm=$scope.chats[i].adi;
				break;
			}
		} //böylelikle veritabanında girilen ilk ismi bulduk
	$state.go("acilis");
}
	


//ok simgesine basınca başlangıç ekranına dönsün
$scope.kapat=function(){
	$window.location.reload();
	$state.go('app.sessions');	
}

//bu aradaki fonksiyonlar ara.html için
$scope.mesajlasma=function(){
	var i=0;
	var t=0;
$rootScope.sayidegeri=0;
	for(i=0;i<$scope.chats.length;i++){
		$scope.elm=$scope.chats[i].adi;
		if($scope.elm==$scope.girilenisim){
			t++;
		}
	}
	if(t!=0){
		$state.go("message");
		$rootScope.sayidegeri=0;
	}else{
		$rootScope.sayidegeri=1;
		$state.go("gidilecekmsj");
/*	mesajları silme kısmı
var ref=new Firebase("https://mesajuygulama-ea72c.firebaseio.com");//burada ise firebase yazılan verileri siliyoruz
ref.remove();*/
	}
}

/*
ara.html sayfasındaki arkadaşekle butonunu çalıştırır
$scope.girad=$rootScope.isim1;
	$scope.girnum=$rootScope.num1;
	$scope.girdep=$rootScope.depart1;
	
	$scope.yazi=""; //ara.html sayfasında kullanmak için tanımlı
$scope.arkadaskaydi=function(){ //arkadaslarim tablosunun verilerini burdan yolluyoruz
$scope.yazi="arkadas olarak eklendi"; //fonksiyon içinde tanımlandığndan demekki arkadaşekle butonuna tıklandı
	$http.post("http://localhost/newproject/arkadas.php", {'Password':$scope.girnum, 'Username':$scope.girad,'department':$scope.girdep})
	
    .success(function (data, status, headers, config) {
    });
	}
*/


	
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

/*.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})*/
//çogul olan playlists yerine bu yazıldı
.controller('SessionsCtrl',function($scope,Session){
	
})
/*
.controller('PlaylistCtrl', function($scope, $stateParams) {
})
*/
//tekil olan playlist yerinede
.controller('SessionCtrl',function($scope,$stateParams,$rootScope,$state,sessionservice){
	$scope.ad=$rootScope.isim;
    $scope.soy=$rootScope.sifre; 
	$rootScope.glbal=$rootScope.sifre;
})

//tablolara kayıt
.controller('newInvoice', function ($scope, $http,$rootScope,$state) {

    $scope.addNewInvoice = function (add) {
   var data = {

    Username:$scope.newInvoice.Username,

    Password:$scope.newInvoice.Password,

	department:$scope.newInvoice.department
} 

$http.post("http://localhost/newproject/mysql_database.php", {'Password': $scope.newInvoice.Password, 'Username': $scope.newInvoice.Username,'department': $scope.newInvoice.department})

    .success(function (data, status, headers, config) {

        console.log($scope.newInvoice.Username);

		console.log($scope.newInvoice.department);
    });
	
}

}) 
//üye-kontrol
.controller('ctrl',function($scope,$http,$state,$ionicLoading,$rootScope,MyService){	
var te=MyService.islogged();//bu kısımda şunu kontrol ediyoruz eger değerler silinmemişse yani logout butonuna basılmadan çıkılmışsa
		if(te){//session özelliği aktif olarak devam eder
		
			$state.go('app.sessions');
		}
$scope.boyut=0;
$rootScope.sayi=0;
$scope.tekrar=0;

	$scope.yap=function(){
		$rootScope.searchgirilen=$scope.ctrl.Username;
  $rootScope.isim=$scope.ctrl.Username;
 $rootScope.sifre=$scope.ctrl.Password; 
   $http({method: 'GET', url:"http://localhost/newproject/liste.php"})
   .success(function(response) {
        $scope.mydata=response;
		$scope.boyut=$scope.mydata.length;//dizi boyutu
	for(a=0;a<$scope.boyut;a++){
		$scope.tut=$scope.mydata[a].name;
		$scope.tut1=$scope.mydata[a].number;
		if($scope.tut==$scope.isim){
			if($scope.tut1==$scope.sifre){
				$scope.sayi=1;
		}}
	}
	if($scope.sayi==0){
		MyService.degerKontrol($scope.sayi,$scope.isim);
		alert("boyle bir kullanıcı yok ekleyebilirsiniz");
				$scope.ctrl.Username="";
		$scope.ctrl.Password="";
		$state.go("search");
	}
	else{
			alert("girilen özelliklerde kullanıcı var");	
			MyService.degerKontrol($scope.sayi,$scope.isim);//girilen isme göre oturum açıldı
			$scope.sayi=0;//degeri sıfırladımki surekli doğru demesin
		//$scope.ctrl.Username="";//deger girilen alanı tekrar boş hale getir
		//$scope.ctrl.Password="";//deger girilen alanı tekrar boş hale getirir
	    $state.go('app.sessions');//eger yapılan işlem doğruysa bu sayfaya yönlendir
	}
    });	
	}
})
/*ara.html sayfası control
.controller('aractrl',function($scope,$rootScope,$http,$state){ //açılan sayfada bilgileri gösterme
	$scope.girad=$rootScope.isim1;
	$scope.girnum=$rootScope.num1;
	$scope.girdep=$rootScope.depart1;
	
	$scope.yazi=""; //ara.html sayfasında kullanmak için tanımlı
$scope.arkadaskaydi=function(){ //arkadaslarim tablosunun verilerini burdan yolluyoruz
$scope.yazi="arkadas olarak eklendi"; //fonksiyon içinde tanımlandığndan demekki arkadaşekle butonuna tıklandı
	$http.post("http://localhost/newproject/arkadas.php", {'Password':$scope.girnum, 'Username':$scope.girad,'department':$scope.girdep})
	
    .success(function (data, status, headers, config) {
    });
	}
	$scope.mesajlasma=function(){
	$state.go("message");
	}
}) 
*/
/* bu şimdilik kullanılmıyor
.controller('acilisctrl',function($scope,$state,$rootScope,MyService){
	var te=MyService.islogged();//bu kısımda şunu kontrol ediyoruz eger değerler silinmemişse yani logout butonuna basılmadan çıkılmışsa
		if(te){//session özelliği aktif olarak devam eder
			$state.go('app.sessions');
		}
	$scope.basla=function(){
		$rootScope.yenigir=$scope.acilisctrl.Username;
		if($rootScope!=""){//input boş kalmasın diye
		MyService.degerKontrol(1,$rootScope.yenigir);//ilk sayfadan girilen degere göre
		$state.go('app.sessions');}
	}
}) */
.controller('brws',function($state,$scope,$rootScope,$http){	
$rootScope.alinan="deger";
$rootScope.kac=0;
	$scope.yeni=[];//length hatası vermesin diye
	  $scope.yeniler=function(){
	   $http({method: 'GET', url:"http://localhost/newproject/baglan.php"})
   .success(function(resp) {
	   var deger=resp;
		  var i;
		  for(i=0;i<deger.length;i++){
			  // console.log(deger[i].name.charAt(0)); ilk harfini alıyoruz burda
			   $scope.yeni[i]=deger[i].name;
			   }
   });
    return $scope.yeni;//geridönüş
	  }
	$scope.bilgiler=function(){
		$rootScope.kac+=1;
		console.log($rootScope.kac);
			  $rootScope.isim1=$scope.brws.Username;//formlardaki veriyi alıyoruz
      $rootScope.depart1=""; 
	  $rootScope.num1="";
$http({method: 'GET', url:"http://localhost/newproject/liste.php"})
   .success(function(response) {
        $scope.mydata=response; //sinif tablosundaki tüm değerler mydata adlı dizinin içinde
 	$scope.boyut=$scope.mydata.length;//dizi boyutu
	for(a=0;a<$scope.boyut;a++){
		$scope.tut=$scope.mydata[a].name;
		//$scope.tut1=$scope.mydata[a].number;
		if($scope.tut==$scope.isim1){
				$scope.sayi=1;
				$rootScope.num1=$scope.mydata[a].number;//girile isim dogru ise numarasını alıyoruz
				$rootScope.depart1=$scope.mydata[a].department; //eger girilen isim doğru ise bölüm adını da hemen ögreniyoruz
		}
	}
	if($scope.sayi==1){
			$scope.sayi=0;
			$state.go("ara");
	}
    }); 
	}
});

















